# Translation for Idioma
# Copyright (C) 2013 Brandon Invergo
# This file is distributed under the same license as the Idioma package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-06-24 22:15+0200\n"
"PO-Revision-Date: 2013-06-24 22:30+0200\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

# {0} is the box number, {1} is the total number of cards in the box
#: ../idioma:55
msgid "Box {0}: {1} cards to test"
msgstr ""

# {0} is the total number of correct cards, {1} is the total number of
# cards, {2} is the percent of cards correct)
#: ../idioma:77
msgid "{0} / {1} ({2}%) correct"
msgstr ""

# {0} is the box number
#: ../idioma:85
msgid "Box {0}"
msgstr ""

# {0} is the total number of cards
#: ../idioma:113
msgid "Total number of cards: {0}"
msgstr ""

# Do not change the key characters (q, r)
#: ../idioma:119
msgid "Enter 'q' at any time to quit and 'r' at any time to start over."
msgstr ""

#: ../idioma:122
msgid "Box #(1-5): "
msgstr ""

#: ../idioma:123
msgid "Invalid box number"
msgstr ""

#: ../idioma:124
msgid "Card #(1-{0}): "
msgstr ""

#: ../idioma:125
msgid "Invalid card number"
msgstr ""

# Do not change the key characters (e, d, c)
#: ../idioma:126
msgid "Action (e: edit, d: delete, c: change box): "
msgstr ""

#: ../idioma:127
msgid "New box #(1-5): "
msgstr ""

#: ../idioma:128
msgid "Invalid action"
msgstr ""

# {0} is the name of a mode, as entered by the user
#: ../idioma:244
msgid "\"{0}\" mode on all modules is not supported."
msgstr ""

# {0} is the name of some nonexistent module entered by the user
#: ../idioma:262
msgid "\"{0}\" module does not exist."
msgstr ""

# The user must simply hit ENTER without typing adding any characters to
# quit
#: ../idioma:278
msgid "Create a blank entry to quit."
msgstr ""

#: ../idioma:298
msgid ""
"Idioma {0}\n"
"Copyright (C) 2011, 2012, 2013 Brandon Invergo\n"
"License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl."
"html>.\n"
"This is free software: you are free to change and redistribute it.\n"
"There is NO WARRANTY, to the extent permitted by law."
msgstr ""

# Do not translate the mode and module names
#: ../idioma:307
msgid ""
"Usage: idioma MODE LANGUAGE MODULE\n"
"\n"
"MODE:\n"
"    add        Add cards to the module's card box\n"
"    test       Test your knowledge of the module's cards\n"
"    dump       Print the contents of the module's card box\n"
"    reset      Move all the cards in the module's card box back to Box 1\n"
"    clear      Remove all the cards from the module's card box\n"
"    edit       Edit the cards of a module\n"
"    \n"
"MODULE:\n"
"    vocab      Translate words to and from English\n"
"    conj       Conjugate verbs\n"
"    lists      Keep lists of related words (ie articles, pronouns)\n"
"    all        All available modules (unavailable in 'add' and 'edit' "
"modes)\n"
"\n"
"LANGUAGE is arbitrary and is merely used to differentiate between card "
"boxes.\n"
"\n"
"Examples:\n"
"    Add Spanish vocabulary:    \n"
"    $ idioma add spanish vocab \n"
"\n"
"    Test German conjugation:\n"
"    $ idioma test german conj\n"
"\n"
"Report bugs to: brandon@invergo.net\n"
"Idioma home page: <http://idioma.invergo.net>"
msgstr ""

# This is the name of the user's native language, as written in that language
#: ../src/idioma/__init__.py:52
msgid "English"
msgstr ""

# The user is prompted to confirm that some input is correct.  Include
# an abbreviation for "yes" and "no", in that order, with "yes" being
# capitalized or otherwise emphasized to show that it is the default
# response.
#: ../src/idioma/__init__.py:53
msgid "OK (Y/n)? "
msgstr ""

# Lower-case only
#: ../src/idioma/__init__.py:54
msgid "yes"
msgstr ""

# ENTER refers to the keyboard key, which is pressed to skip adding a
# new value for something when editing a card
#: ../src/idioma/__init__.py:55
msgid "Hit ENTER to keep an old value."
msgstr ""

#: ../src/idioma/__init__.py:56
msgid "Correct"
msgstr ""

#: ../src/idioma/__init__.py:57
msgid "Incorrect"
msgstr ""

# {0} is replaced with some card's name
#: ../src/idioma/__init__.py:58
msgid "\"{0}\" added to card box"
msgstr ""

# {0} is replaced with some card's name
#: ../src/idioma/__init__.py:59
msgid "\"{0}\" discarded"
msgstr ""

#: ../src/idioma/__init__.py:60
msgid "file corrupted"
msgstr ""

# This is the Conjugation module's name; all lower-case
#: ../src/idioma/conjugation.py:35
msgid "conjugation"
msgstr ""

# {0} is replaced with a language name
#: ../src/idioma/conjugation.py:36
msgid "{0} infinitive: "
msgstr ""

#: ../src/idioma/conjugation.py:37
msgid "Verb tense: "
msgstr ""

# {0} is replaced with a language name
#: ../src/idioma/conjugation.py:42
msgid "Define {0} personal pronouns:"
msgstr ""

# First-person
#: ../src/idioma/conjugation.py:44
msgid "I"
msgstr ""

# Singular second-person
#: ../src/idioma/conjugation.py:45
msgid "you (sing.)"
msgstr ""

# Singular third-person.  Multiple words may be listed as necessary.
#: ../src/idioma/conjugation.py:46
msgid "he/she/it"
msgstr ""

#: ../src/idioma/conjugation.py:47
msgid "we"
msgstr ""

# Plural second-person
#: ../src/idioma/conjugation.py:48
msgid "you (plur.)"
msgstr ""

# Plural third-person.  Multiple words may be listed, i.e. in Spanish: ellos/ellas
#: ../src/idioma/conjugation.py:49
msgid "they"
msgstr ""

# {0} is replaced with a language name, {1} is replaced with an old
# value to be replaced
#: ../src/idioma/conjugation.py:111
msgid "{0} infinitive ({1}): "
msgstr ""

# {0} is replaced with the old value of a verb tense to be replaced
#: ../src/idioma/conjugation.py:112
msgid "Verb tense ({0}): "
msgstr ""

# The name of the Vocabulary module; all lower-case
#: ../src/idioma/vocabulary.py:34
msgid "vocabulary"
msgstr ""

# An adjective, as in an alternate value
#: ../src/idioma/vocabulary.py:35
msgid "Alternate"
msgstr ""

# "no" is the default response and should be emphasized as so
#: ../src/idioma/vocabulary.py:38
msgid "Alternate forms (i.e. plural) (y/N)?"
msgstr ""

# The title of a list
#: ../src/idioma/wordlists.py:33
msgid "List title"
msgstr ""

# The user may enter some description of the word list, in the case of
# alternate forms, one list per form (such as the accusative or
# nominative forms in German)
#: ../src/idioma/wordlists.py:36
msgid "Form (optional)"
msgstr ""

#: ../src/idioma/wordlists.py:69
msgid "Form"
msgstr ""
