#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  conjugation.py
#       
#  Copyright 2011, 2013 Brandon Invergo <brandon@invergo.net>
#       
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 3 of the
#  License, or (at your option) any later version.
#       
#  This program is distributed in the hope that it will be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  General Public License for more details.
#       
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#  02110-1301, USA.


from __future__ import print_function

import os
import pickle
import time
import sys

from idioma import IdiomaModule, _


class Conjugation(IdiomaModule):
    module_name = _("conjugation")
    inf_msg = _("{0} infinitive: ")
    tense_msg = _("Verb tense: ")
    
    def __init__(self, card_box_file, language):
        self.card_box_file = card_box_file
        self.language = language
        define_msg = _("Define {0} personal pronouns:")
        define_msg_fmt = define_msg.format(language)
        pron_i = _("I")
        pron_y_s = _("you (sing.)")
        pron_hsi = _("he/she/it")
        pron_w = _("we")
        pron_y_p = _("you (plur.)")
        pron_t = _("they")
        if os.path.exists(self.card_box_file):
            self.load_card_box()
        else:
            print(define_msg)
            nat_pronouns = [pron_i, pron_y_s, pron_hsi, pron_w,
                            pron_y_p, pron_t]
            self.pronouns = []
            for nat_pronoun in nat_pronouns:
                pronoun = raw_input("{0}: ".format(nat_pronoun))
                self.pronouns.append(pronoun)
            self.card_box = [[], [], [], [], []]

    def load_card_box(self):
        corrupt_msg = "{0} {1} {2}".format(self.language,
                                           self.module_name, self.corrupt_msg)
        try:
            with open(self.card_box_file, "rb") as card_box_handle:
                self.card_box, self.pronouns = pickle.load(card_box_handle)
        except:
            sys.exit(corrupt_msg)

    def save_card_box(self):
        with open(self.card_box_file, "wb") as card_box_handle:
            pickle.dump((self.card_box, self.pronouns), card_box_handle,
                         pickle.HIGHEST_PROTOCOL)

    def add_card(self):
        conj_msg = "{0}: ".format(self.module_name.title())
        print("\n-----\n")
        card = {}
        native = raw_input(self.inf_msg.format(self.nat_name))
        if native == "":
            return False
        foreign = raw_input(self.inf_msg.format(self.language))
        if foreign == "":
            return False
    
        tense = raw_input(self.tense_msg)
        conjugation = [[pronoun, None] for pronoun in self.pronouns]
        print("\n{0}".format(conj_msg))
        for pronoun in conjugation:
            pronoun[1] = raw_input("{0} ".format(pronoun[0]))
        cur_time = time.localtime()
        year = cur_time.tm_year
        day = cur_time.tm_yday
        exp_day = year * 365.25 + day
        card = {self.nat_name: native, self.language: foreign,
                "tense": tense, "conjugation": conjugation, 
                "day": exp_day}
        finished = raw_input(self.finished_msg)
        if finished == "" or finished.lower() in self.yes:
            added_msg_fmt = self.added_msg.format(card[self.language])
            print(added_msg_fmt)
            self.card_box[0].append(card)
            self.save_card_box()
        else:
            disc_msg_fmt = self.disc_msg.format(card[self.language])
            print(disc_msg_fmt)
        return True
        
    def edit_card(self, card):
        edit_inf_msg = _("{0} infinitive ({1}): ")
        edit_tense_msg = _("Verb tense ({0}): ")
        print(self.old_msg)
        native = raw_input(self.inf_msg.format(card[self.nat_name]))
        if native != "":
            card[self.nat_name] = native
        foreign = raw_input(edit_inf_msg.format(self.language,
                                                card[self.language]))
        if foreign != "":
            card[self.language] = foreign
        tense = raw_input(edit_tense_msg.format(card["tense"]))
        if tense != "":
            card["tense"] = tense
        for pronoun in card["conjugation"]:
            conj = raw_input('{0} ({1}) '.format(pronoun[0], pronoun[1]))
            if conj != "":
                pronoun[1] = conj
        finished = raw_input(self.finished_msg)
        if finished == "" or finished.lower() in self.yes:
            return card
        else:
            return None

    def print_card(self, card):
        line_len = 50
        text_len = 35
        word_len = 10
        pronoun_len = 0
        for pronoun in self.pronouns:
            if len(pronoun) > pronoun_len:
                pronoun_len = len(pronoun)
        if len(self.language) > 7:
            lang_len = len(self.language)
        else:
            lang_len = 7
        fields = [(self.nat_name, self.inf_msg.format(self.nat_name)),
                  (self.language,
                    self.inf_msg.format(self.language)),
                  ("tense", self.tense_msg)]
        for field in fields:
            field_str = field[1].rjust(lang_len + 11, ' ')
            end_padding = text_len - (len(field_str) + 
                                      len(card[field[0]]) + 3)
            print("{0}: {1}{2}".format(field_str,
                                       card[field[0]],
                                       ' ' * end_padding
                                       ).center(line_len))
        print("~~~~".center(line_len))
        conj = card["conjugation"]
        for i in range(3):
            pron1 = conj[i][0].rjust(pronoun_len, ' ')
            pron2 = conj[i + 3][0].rjust(pronoun_len, ' ')
            padding1 = word_len - len(conj[i][1])
            padding2 = word_len - len(conj[i + 3][1])
            padding = text_len - (len(pron1) + len(conj[i][1]) +
                            len(pron2) + len(conj[i + 3][1]) + 2)
            print("{0} {1}{2}    {3} {4}{5}".format(
                pron1,
                conj[i][1],
                ' ' * padding1,
                pron2,
                conj[i + 3][1],
                ' ' * padding2
                ).center(line_len))

    def test_card(self, card):
        correct = True
        print("{0} ({1}): {2}".format(card[self.language],
                                            card[self.nat_name],
                                            card["tense"]))
        print("------")
        for  pronoun in card["conjugation"]:
            response = raw_input("{0} ".format(pronoun[0]))
            if response != pronoun[1]:
                print("{0}: {1} {2}\n".format(self.incorrect_msg,
                                             pronoun[0], pronoun[1]))
                correct = False
            else:
                print("{0}\n".format(self.correct_msg))
        return correct
