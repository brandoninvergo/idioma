#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  vocabulary.py
#       
#  Copyright 2011, 2013 Brandon Invergo <brandon@invergo.net>
#       
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 3 of the
#  License, or (at your option) any later version.
#       
#  This program is distributed in the hope that it will be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  General Public License for more details.
#       
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#  02110-1301, USA.


from __future__ import print_function

import time
import random
import copy

from idioma import IdiomaModule, _


class Vocabulary(IdiomaModule):
    module_name = _("vocabulary")
    alt_msg = _("Alternate")
        
    def add_card(self):
        alt_prompt = _("Alternate forms (i.e. plural) (y/N)?")
        print("\n-----\n")
        native = raw_input("{0}: ".format(self.nat_name)).strip()
        if native == "":
            return False
        foreign = raw_input("{0}: ".format(self.language)).strip()
        if foreign == "":
            return False
        do_alts = raw_input("\n{0} ".format(alt_prompt))
        alts = []
        if do_alts.lower() in self.yes:
            while True:
                alt_native = raw_input("\t{0} {1}: ".format(self.alt_msg,
                                                            self.nat_name)).strip()
                if alt_native == '':
                    break
                alt_foreign = raw_input("\t{0} {1}: ".format(self.alt_msg,
                                                             self.language)).strip()
                alts.append({self.nat_name: alt_native,
                             self.language: alt_foreign})
        cur_time = time.localtime()
        year = cur_time.tm_year
        day = cur_time.tm_yday
        exp_day = year * 365.25 + day
        card = {"normal": {self.nat_name: native, self.language: foreign}, 
                "alternates": alts,
                "day": exp_day}
        finished = raw_input(self.finished_msg)
        if finished == "" or finished.lower() in self.yes:
            print(self.added_msg.format(foreign))
            self.card_box[0].append(card)
            self.save_card_box()
        else:
            print(self.disc_msg.format(foreign))
        return True
        
    def edit_card(self, card):
        print(self.old_msg)
        native = raw_input('{0} ({1}): '.format(self.nat_name,
                                                card['normal'][self.nat_name])).strip()
        if native != '':
            card['normal'][self.nat_name] = native
        foreign = raw_input('{0} ({1}): '.format(
            self.language, card['normal'][self.language])).strip()
        if foreign != '':
            card['normal'][self.language] = foreign
        alts = card['alternates']
        if len(alts) > 0:
            for alt in alts:
                alt_native = raw_input(
                    "{0} {1} ({2}): ".format(self.alt_msg, self.nat_name,
                                             alt[self.nat_name]))
                if alt_native != "":
                    alt[self.nat_name] = alt_native
                alt_foreign = raw_input(
                    "{0} {1} ({2}): ".format(self.alt_msg, self.language,
                                             alt[self.language]))
                if alt_foreign != "":
                    alt[self.language] = alt_foreign
        finished = raw_input(self.finished_msg)
        if finished == "" or finished.lower() in self.yes:
            return card
        else:
            return None

    def print_card(self, card):
        word_len = 26
        normal = card['normal']
        alts = card['alternates']
        print('{0} <--> {1}'.format(normal[self.nat_name].rjust(word_len),
                                  normal[self.language].ljust(word_len)))
        for alt in alts:
            print('{0} <--> {1}'.format(
                ''.join(['(', alt[self.nat_name]]).rjust(word_len),
                ''.join([alt[self.language], ')']).ljust(word_len)))

    def test_card(self, card):
        languages = [self.nat_name, self.language]
        random.shuffle(languages)
        versions = copy.copy(card['alternates'])
        versions.append(card['normal'])
        version = random.sample(versions, 1)[0]
        query = version[languages[0]]
        answer = version[languages[1]]
        response = raw_input('[{0}-->{1}] {2} = '.format(languages[0],
                                                         languages[1], 
                                                         query))
        if response == answer:
            print(self.correct_msg)
            return True
        else:
            print('{0}: {1}'.format(self.incorrect_msg, answer))
            return False
