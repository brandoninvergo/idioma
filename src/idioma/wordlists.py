#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  wordlists.py
#       
#  Copyright 2011, 2013 Brandon Invergo <brandon@invergo.net>
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 3 of the
#  License, or (at your option) any later version.
#       
#  This program is distributed in the hope that it will be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  General Public License for more details.
#       
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#  02110-1301, USA.


from __future__ import print_function

import time

from idioma import IdiomaModule, _


class WordLists(IdiomaModule):
    module_name = 'wordlists'
    title_msg = _("List title")

    def add_card(self):
        form_msg = _("Form (optional)")
        print("\n-----\n")
        card = {}
        card["title"] = raw_input("{0}: ".format(self.title_msg))
        if card["title"] == "":
            return False
        card["form"] = raw_input("{0}: ".format(form_msg))
        print("------")
        card["list"] = []
        while True:
            native = raw_input("{0}: ".format(self.nat_name))
            if native == "":
                break
            foreign = raw_input("{0}: ".format(self.language))
            if foreign == "":
                break
            card["list"].append([native, foreign])
        cur_time = time.localtime()
        year = cur_time.tm_year
        day = cur_time.tm_yday
        exp_day = year * 365.25 + day
        card["day"] = exp_day
        finished = raw_input(self.finished_msg)
        card_desc = "{0} ({1})".format(card["title"], card["form"])
        if finished == "" or finished.lower() in self.yes:
            print(self.added_msg.format(card_desc))
            self.card_box[0].append(card)
            self.save_card_box()
        else:
            print(self.disc_msg.format(card_desc))
        return True

    def edit_card(self, card):
        form_msg = _("Form")
        print(self.old_msg)
        title = raw_input("{0} ({1}): ".format(self.title_msg, card["title"]))
        if title != "":
            card["title"] = title
        form = raw_input("{0} ({1}): ".format(form_msg, card["form"]))
        if form != "":
            card["form"] = form
        for item in card["list"]:
            native = raw_input("{0} ({1}): ".format(self.nat_name, item[0]))
            if native != "":
                item[0] = native
            foreign = raw_input("{0} ({1}): ".format(self.language, item[1]))
            if foreign != "":
                item[1] = foreign
        finished = raw_input(self.finished_msg)
        if finished == "" or finished.lower() in self.yes:
            return card
        else:
            return None

    def print_card(self, card):
        line_len = 50
        text_len = 35
        word_len = 10
        item_len = 15
        if card["form"] == "":
            print(card["title"].center(line_len))
        else:
            print("{0} ({1})".format(card["title"],
                                     card["form"]).center(line_len))
        print("~~~~".center(line_len))
        for item in card["list"]:
            native = item[0].rjust(item_len)
            padding = text_len - (len(native) + len(item[1]) + 2)
            print("{0}: {1}{2}".format(
                native,
                item[1],
                ' ' * padding).center(line_len))

    def test_card(self, card):
        correct = True
        print("{0} ({1})".format(
            card["title"],
            card["form"]))
        print("------")
        for  item in card["list"]:
            response = raw_input("{0}: ".format(item[0]))
            if response != item[1]:
                print('{0}: {1}: {2}\n'.format(self.incorrect_msg,
                                               item[0], item[1]))
                correct = False
            else:
                print("{0}\n".format(self.correct_msg))
        return correct
