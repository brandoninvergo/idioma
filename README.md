# Introduction

Idioma is a not-entirely-user-friendly program for studying foreign
languages. There are nicer programs out there with GUIs like Anki or
Ignuit but I found myself in a foreign country with no internet
connection and a pressing need to learn a lot of vocabulary and
grammar as quickly and efficiently as possible. Thus, Idioma was born:
quick and to-the-point, built strictly for getting the job done. In
the end, though, it has ended up quite flexible.

See the included documentation for more information.

# Contributing

Patches are always welcome.  New modules are even more welcome.
Lastly, Idioma should be translated to other languages to make it less
Anglo-centric.

# Author

Brandon Invergo <brandon@invergo.net

# Web

http://idioma.invergo.net

# License

Idioma is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.
       
Idioma is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Idioma.  If not, see <http://www.gnu.org/licenses/>.
